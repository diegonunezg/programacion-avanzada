from abc import ABC
from abc import abstractmethod

# Se define la clase abstracta Fighter
class Fighter(ABC):

	@abstractmethod
	def toString(self):
		pass


	@property
	@abstractmethod
	def hp(self):
		pass


	@property
	@abstractmethod
	def tipo(self):
		pass


	@abstractmethod
	def on_hit(self):
		pass


	@property
	@abstractmethod
	def isVulnerable(self):
		pass


	@abstractmethod
	def damagePoints(self, other):
		pass


	@abstractmethod
	def end_turn(self):
		pass
