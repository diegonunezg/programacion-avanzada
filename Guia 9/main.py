# By Diego Núñez González

"""

El modulo multipledispatch no pertenece a la libreria estandar y,
por lo tanto, debe ser instalado para la correcta ejecucion del codigo.

"""

from fighter import Fighter
from warrior import Warrior
from wizard import Wizard
from multipledispatch import dispatch
import random


# Si un guerrero ataca a otro guerrero siempre hace 6 de daño,
# ya que los guerreros nunca son vulnerables.
@dispatch(Warrior, Warrior)
def atk_action(atk, dfn):
	dfn.on_hit(6)


# Si un guerrero ataca a un mago, dependiendo de la
# vulnerabilidad del mago, le hace mas o menos daño.
@dispatch(Warrior, Wizard)
def atk_action(atk, dfn):
		damage = atk.damagePoints(dfn)
		dfn.on_hit(damage)


# Si un mago ataca a un guerrero, dependiendo de si el
# mago preparo un hechizo, hace mas o menos daño.
@dispatch(Wizard, Warrior)
def atk_action(atk, dfn):
	damage = atk.damagePoints()
	dfn.on_hit(damage)


# Si un mago ataca a otro mago, el daño depende
# de si el atacante preparo un hechizo o no.
@dispatch(Wizard, Wizard)
def atk_action(atk, dfn):
	damage = atk.damagePoints()
	dfn.on_hit(damage)


def main():

	# Para demostrar del ejercicio, se eligen 2 luchadores de una clase al azar.
	print("\n")
	j1 = random.choice((Wizard(), Warrior()))
	print("Jugador 1", j1.toString())
	j2 = random.choice((Wizard(), Warrior()))
	print("Jugador 2", j2.toString())
	print("\n")

	# Mientras los 2 luchadores tengan vida restante, seguiran peleando
	while j1.hp > 0 and j2.hp > 0:
		# Si el jugador 1 y/o el jugador 2 es de tipo Wizard,
		# hay una probabilidad del 50% de que prepare un hechizo.
		if isinstance(j1, Wizard):
			if random.choice((True, False)):
				j1.prepareSpell()

		if isinstance(j2, Wizard):
			if random.choice((True, False)):
				j2.prepareSpell()

		# Luego ambos luchadores se atacan a la vez
		atk_action(j1, j2)
		atk_action(j2, j1)

		# Ambos jugadores terminan de atacar, y sus caracteristicas se resetean
		j1.end_turn()
		j2.end_turn()

		# Se imprime por pantalla los tipos de luchador y la vida restante
		print(f"J1 {j1.tipo} hp: {j1.hp} - J2 {j2.tipo} hp: {j2.hp}")



if __name__ == "__main__":
	main()
