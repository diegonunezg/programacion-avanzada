from fighter import Fighter

# Se define la clase Wizard a partir de la clase abstracta Fighter
class Wizard(Fighter):
	# Constructor
	def __init__(self):
		self._tipo = "Mago"
		self._hp = 100
		self._vulnerable = False
		self._spell_ready = False


	# Este metodo retorna una descripcion de la clase
	def toString(self):
		return "Fighter es un Wizard"


	# Retorna una string que indica la vida del objeto
	@property
	def hp(self):
		return self._hp


	# Retorna una string que indica el tipo de la clase
	@property
	def tipo(self):
		return self._tipo


	# Invierte el valor booleano del atributo _vulnerable
	def vulnerable(self):
		self._vulnerable = not self._vulnerable


	# Invierte el valor booleano del atributo _spell_ready
	def prepareSpell(self):
		self._spell_ready = not self._spell_ready


	# Retorna el valor de la variable _vulenrable
	@property
	def isVulnerable(self):
		if not self._spell_ready:
			if not self._vulnerable:
				self.vulnerable()
		return self._vulnerable


	# Calcula y retorna la cantidad de daño que inflinge
	def damagePoints(self):
		if self._spell_ready:
			return 12
		else:
			return 3


	# Disminuye la vida del objeto al recibir un impacto
	def on_hit(self, damage):
		self._hp -= damage


	# Se reestablecen las caracteristicas al final de cada turno
	def end_turn(self):
		self._vulnerable = False
		self._spell_ready = False
