from fighter import Fighter

# Se define la clase Warrior a partir de la clase abstracta Fighter
class Warrior(Fighter):
	# Constructor
	def __init__(self):
		self._tipo = "Guerrero"
		self._hp = 80


	# Este metodo retorna una descripcion de la clase
	def toString(self):
		return "Fighter es un Warrior"


	# Retorna una string que indica la vida del objeto
	@property
	def hp(self):
		return self._hp


	# Retorna una string que indica el tipo de la clase
	@property
	def tipo(self):
		return self._tipo


	# Los guerreros nunca son vulnerables
	@property
	def isVulnerable(self):
		return False


	# Calcula y retorna la cantidad de daño que inflinge
	def damagePoints(self, oponent):
		if oponent.isVulnerable:
			return 10
		else:
			return 6


	# Disminuye la vida del objeto al recibir un impacto
	def on_hit(self, damage):
		self._hp -= damage


	# Cuando termina su turno, no necesita realizar ninguna accion adicional
	def end_turn(self):
		pass
