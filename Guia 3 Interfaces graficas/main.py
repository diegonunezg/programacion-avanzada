# By: Magdalena Lolas y Diego Núñez

import pandas
import os

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio

import matplotlib as mpl

mpl.use('Agg')
import matplotlib.pyplot as plt

from graph_options_window import GraphOptions


# Ventana principal de la aplicacion
class MainWindow(Gtk.Window):
	# Constructor
	def __init__(self):
		# Se llama al constructor de la super clase
		super().__init__()
		# Se le establece un tamaño por defecto a la ventana
		self.set_default_size(1100, 480)
		# Se establece como falso la capacidad de redimensionar la ventana
		self.set_resizable(False)
		# Si la ventana se destruye, la ejecucion de la aplicacion termina
		self.connect("destroy", Gtk.main_quit)

		# Se crea un objeto contenedor Gtk.Grid
		self.grid = Gtk.Grid()
		self.grid.set_row_spacing(6)
		# Se añade el contenedor a la ventana
		self.add(self.grid)

		# Se llama al metodo de la clase create_headerbar
		self.create_headerbar()


	# Este metodo crea una headerbar para la ventana
	def create_headerbar(self):
		headerbar = Gtk.HeaderBar()
		# Se le entrega un titulo
		headerbar.set_title("Grapho")
		headerbar.set_show_close_button(True)

		# Se crea un boton para abrir archivos
		open_file_button = Gtk.Button(label="Abrir Archivo")
		# Se conecta el evento clicked con la accion deseada
		open_file_button.connect("clicked", self.open_filechooser)
		# Se añade el boton a la headerbar
		headerbar.pack_start(open_file_button)

		# Se crea un boton para crear un grafico
		new_graph_button = Gtk.Button(label="Crear Grafico")
		# Se conecta el evento clicked con la accion deseada
		new_graph_button.connect("clicked", self.graph_options)
		# Se añade el boton a la headerbar
		headerbar.pack_start(new_graph_button)

		# Se crea un boton para abrir una ventana "acerca de"
		about_button = Gtk.Button()
		# El boton muestra un icono en vez de texto
		icon = Gio.ThemedIcon(name="help-about")
		image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
		about_button.add(image)
		# Se conecta el evento clicked con la accion deseada
		about_button.connect("clicked", self.about_window)
		# Se añade el boton a la headerbar
		headerbar.pack_end(about_button)

		# Se añade la headerbar a la ventana
		self.set_titlebar(headerbar)


	# Este metodo levanta una ventana "acerca de"
	def about_window(self, widget=None):
		dialog = Gtk.AboutDialog()
		dialog.set_title("Acerca de")
		dialog.set_program_name("Grapho")
		dialog.set_version("1.0")
		dialog.set_comments("Visualizador de archivos csv y generador de gráficos")
		dialog.set_website("https://gitlab.com/diegonunezg/programacion-avanzada/-/tree/main/Guia%203%20Interfaces%20graficas")
		dialog.set_website_label("GitLab")
		dialog.set_authors(["Magdalena Lolas", "Diego Núñez"])
		dialog.set_logo_icon_name("document-page-setup")
		dialog.connect('response', lambda dialog, data: dialog.destroy())
		dialog.show_all()


	# Este metodo levanta una ventana de tipo FileChooserDialog
	def open_filechooser(self, widget=None):
		dialog = Gtk.FileChooserDialog(
									   title="Elija un archivo para abrir",
									   parent=self,
									   action=Gtk.FileChooserAction.OPEN
		)
		dialog.add_buttons(
			Gtk.STOCK_CANCEL,
			Gtk.ResponseType.CANCEL,
			Gtk.STOCK_OPEN,
			Gtk.ResponseType.OK,
		)

		# Se añaden filtros de busqueda
		self.add_filters_open(dialog)

		# Se espera una respuesta de la ventana FileChooserDialog
		response = dialog.run()

		# Si la respuesta es del tipo OK se crea un treeview con el archivo seleccionado
		if response == Gtk.ResponseType.OK:
			self.create_treeview(dialog.get_filename())

		# Finalmente se destruye la ventana
		dialog.destroy()

	# Este metodo crea los filtros de busqueda para archivos .csv
	def add_filters_open(self, dialog):
		filter_csv = Gtk.FileFilter()
		filter_csv.set_name("csv")
		filter_csv.add_pattern("*.csv")
		dialog.add_filter(filter_csv)


	# Este metodo levanta una ventana con las opciones del grafico.
	def graph_options(self, widget=None):
		# La aplicacion intenta crear una ventana de tipo GhaphOptions.
		try:
			opc = GraphOptions(self, self.data)

		# Si no hay data, ocurre una excepcion. Esta levanta una ventana de advertencia.
		except AttributeError:
			dialog = Gtk.MessageDialog(
									   parent=self,
									   flags=0,
									   message_type=Gtk.MessageType.WARNING,
									   buttons=Gtk.ButtonsType.CLOSE,
									   text="Imposible crear gráfico sin data"
									   )
			dialog.format_secondary_text("Cargue un archivo e intente de nuevo")
			dialog.run()
			dialog.destroy()

		# La sentencia else ocurre al final del bloque try/except, solo si no ocurren excepciones.
		else:
			opc.show_all()
			# Durante la ejecucion de la ventana levantada, pueden ocurrir errores,
			# por lo que hasta que no se de una respuesta valida, entra en un ciclo.
			while True:
				response = opc.run()
				# La variable flag indica si hay errores o no al momento de obtener los datos.
				flag = 0

				# Si se oprime el boton aceptar de la ventana, se recogen los datos ingresados
				if response == Gtk.ResponseType.OK:
					x = opc.get_x_axis()
					y = opc.get_y_axis()
					distinctive = opc.get_distinctive()
					colormap = opc.get_colormap()
					# En caso de que no se hayan asignado parametros para los
					# ejes del grafico, se levanta una ventana de advertencia.
					if x == "-Seleccione un parametro-" or y == "-Seleccione un parametro-":
						opc.raise_warning()
					else:
						flag = 1
				# Si la respuesta es el boton cancelar o el cerrado de
				# la ventana, esta se destruye y se escapa del ciclo
				else:
					opc.destroy()
					break
				# Si no ocurrieron errores la ventana se destruye y se crea el grafico.
				if flag == 1:
					opc.destroy()
					self.create_image(x, y, distinctive, colormap)
					break


	# Este metodo crea el treeview
	def create_treeview(self, path):
		# El treeview estara contenido dentro de una ScrolledWindow
		scroll = Gtk.ScrolledWindow()
		scroll.set_hexpand(True)
		scroll.set_vexpand(True)
		self.grid.attach(scroll, 0, 0 , 1, 1)
		self.treeview = Gtk.TreeView()
		scroll.add(self.treeview)

		# Se crea el dataframe a partir de cargar el archivo con pandas.
		self.data = pandas.read_csv(path)

		# Si es que existen columnas en el treeview, se eliminan para añadir nuevas
		if self.treeview.get_columns():
			for column in self.treeview.get_columns():
				self.treeview.remove_column(column)

		# Se crea el modelo del treeview
		len_columns = len(self.data.columns)
		model = Gtk.ListStore(*(len_columns * [str]))
		self.treeview.set_model(model=model)

		cell = Gtk.CellRendererText()

		for item in range(len_columns):
			column = Gtk.TreeViewColumn(self.data.columns[item], cell, text=item)
			self.treeview.append_column(column)
			column.set_sort_column_id(item)

		# Se añaden los datos del dataframe
		for item in self.data.values:
			line = [str(x) for x in item]
			model.append(line)

		# Se elimina la segunda columna. Esta contiene un Gtk.Image con el
		# grafico. Esto es, para que al momento de abrir un nuevo archivo,
		# borrar el grafico de la data anterior.
		self.grid.remove_column(1)

		self.show_all()


	# Este metodo genera un grafico a partir de la data y lo muestra en un Gtk.Image
	def create_image(self, x_axis, y_axis, distinctive, colormap='winter'):
		# clf() limpia la figura y cla() limpia los ejes del grafico
		plt.clf()
		plt.cla()

		# En caso de que se decida diferenciar los datos segun un parametro,
		# se evalua si es que estos son numericos o de tipo string. En este
		# ultimo caso, son transformados a numeros. Cada string unico tiene un id.
		if distinctive:

			unique_ids = []
			for info in self.data[f'{distinctive}']:
				if info not in unique_ids:
					unique_ids.append(info)

			# Luego por cada dato en el dataframe, se crea una mascara,
			# donde a cada dato se le asigna un numero respecto a su id.
			mask = []
			if isinstance(unique_ids[0], str):
				for row in self.data[f'{distinctive}']:
					mask.append(unique_ids.index(row))
			else:
				for row in self.data[f'{distinctive}']:
					mask.append(unique_ids.index(row))
			# Se crea una columna auxiliar en el dataframe que
			# sera usada para diferenciar los datos por color.
			self.data['mask'] = mask
			# Se crea el grafico de puntos con la funcion scatter()
			plt.scatter(self.data[x_axis], self.data[y_axis], c=self.data['mask'], cmap=colormap)
			# Se añade una leyenda que permite identificar a que corresponde cada color.
			plt.colorbar(label=f"{distinctive}", orientation="horizontal")
		# En caso de que no se pida una distincion por color, el grafico se crea.
		else:
			plt.scatter(self.data[x_axis], self.data[y_axis])

		# Se le añaden etiquetas a los ejes, dependiendo lo que representen.
		plt.xlabel(x_axis)
		plt.ylabel(y_axis)
		# Se guarda la imagen como un .png
		plt.savefig('./graph.png')

		# Se crea un objeto Gtk.Image a partir del .png del grafico.
		self.image = Gtk.Image.new_from_file("./graph.png")
		# Se elimina la segunda columna de la grid, para
		# eliminar un posible grafico anterior
		self.grid.remove_column(1)
		# Se añade la imagen
		self.grid.attach(self.image, 1, 0, 1, 1)
		self.show_all()


def main():
	win = MainWindow()
	win.show_all()
	Gtk.main()


if __name__ == "__main__":
	main()
