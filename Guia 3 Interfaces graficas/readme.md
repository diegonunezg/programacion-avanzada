# Programación Avanzada - Guía 3 Unidad 2

_Este programa consiste en una aplicación simple que abre archivos de tipo .csv y crea un gráfico a partir de los datos entregados por el archivo._

---

### Prerrequisitos del programa
Para ejecutar la aplicación son necesarias las siguientes librerías:
* [pandas](https://pandas.pydata.org/)
* [matplotlib](https://matplotlib.org/stable/index.html)

---

### Instrucciones de uso
1.  Haga click en el botón Abrir Archivo y seleccione el archivo .csv que desee convertir en un gráfico
2.  Una vez seleccionado el archivo haga click en el botón Abrir y éste se mostrará a la izquierda de la aplicación
3.  Luego haga click en el botón Crear Gráfico
4.  Haga click en -Seleccione un parámetro- y elija una opción para cada uno de los 4 parámetros
5.  Cuando tenga todos los parámetros seleccionados haga click en el botón Aceptar
6.  Se mostrará su gráfico a la derecha de la aplicación
7.  Para cerrar la aplicación, presione el botón x en la esquina superior derecha
---

## MainWindow

La clase MainWindow, representa una ventana, que hereda de Gtk.Window a traves del método _super().\_\_init\_\_()_. A partir de métodos propios de la clase Gtk.Window, se puede dar formato a la ventana, como por ejemplo, darle un titulo o cambiar el tamaño por defecto.

Dentro de esta ventana, se encuentran contenidos otros objetos de Gtk, como lo son Gtk.HeaderBar, Gtk.Button, Gtk.ScrolledWindow, Gtk.TreeView, Gtk.Image, entre otros.

### HeaderBar
La clase Gtk.HeaderBar representa un objeto tipo barra de encabezado, que sobrescribe a la que viene por defecto. Esto permite otorgar una mayor personalización a la misma, permitiendo añadir botones por ejemplo.

### TreeView
La clase Gtk.TreeView representa un objeto que, muestra en forma de tabla los datos entregados. Para ello, a través de la librería pandas, se crea un dataframe, que contiene cada una de las columnas  y datos asociados, extraídos de un archivo _.csv_. Luego se crea un Gtk.ListStore que sirve de modelo para el treeview.

### Image
La clase Gtk.Image representa un objeto que muestra una imagen a partir de un archivo con formato de imagen (.jpg, .png, etc.)

En la aplicación, se utiliza para mostrar la imagen del grafico generado.

### ComboBoxText

Un ComboBoxText es un widget que le permite al usuario escoger una opción de una lista. Hereda sus métodos de Gtk.Bin, Gtk.CellEditable y Gtk.CellLayout. En el código se utilizaron 4 comboboxes:
- Eje x: Selecciona el parámetro para el eje y.
- Eje y: Selecciona el parámetro para el eje y.
- Diferenciador: Selecciona el parámetro para diferenciar por colores, por ejemplo, si el parámetro es el sexo, se diferenciará con 2 colores que representarán el sexo femenino y masculino.
- Mapa de color: permite elegir entre 5 mapas de colores (winter, spring, summer, autumn y cool) para los datos del gráfico.

---
### Integrantes
* Magdalena Lolas [mlolas](https://gitlab.com/mlolas)
* Diego Núñez [diegonunezg](https://gitlab.com/diegonunezg)
