import pandas
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


# Ventana de las opciones del grafico
class GraphOptions(Gtk.Dialog):
	# Constructor
	def __init__(self, parent, dataframe):
		# Se llama al constructor de la super clase.
		super().__init__()
		# Se le otorgan caracteristicas por defecto.
		self.set_default_size(600, 240)
		self.set_resizable(False)
		self.set_transient_for(parent)
		self.set_title("Opciones del Grafico")
		self.set_border_width(20)

		# Se añaden botones a la ventana
		self.add_buttons(
						 Gtk.STOCK_CANCEL,
						 Gtk.ResponseType.CANCEL,
						 Gtk.STOCK_OK,
						 Gtk.ResponseType.OK
						)

		# Se crean los contenedores de la ventana.
		self.box = self.get_content_area()
		self.box.set_orientation(Gtk.Orientation.VERTICAL)
		self.grid = Gtk.Grid()
		self.grid.set_column_spacing(6)
		self.grid.set_column_homogeneous(True)

		# Se recopilan las columnas del dataframe
		features = [x for x in dataframe.columns if x.istitle()]

		# Se crean 4 comboboxtext cada una asociada a una opcion del grafico.
		self.comboboxtext1 = Gtk.ComboBoxText()
		self.comboboxtext1.append("DEFAULT", "-Seleccione un parametro-")
		self.comboboxtext1.set_active_id("DEFAULT")

		for feature in features:
			self.comboboxtext1.append(f"{feature.upper()}", feature)

		self.grid.attach(Gtk.Label(label="Eje x"), 0, 0, 1, 1)
		self.grid.attach(self.comboboxtext1, 1, 0, 1, 1)

		self.comboboxtext2 = Gtk.ComboBoxText()
		self.comboboxtext2.append("DEFAULT", "-Seleccione un parametro-")
		self.comboboxtext2.set_active_id("DEFAULT")
		for feature in features:
			self.comboboxtext2.append(f"{feature.upper()}", feature)

		self.grid.attach(Gtk.Label(label="Eje y"), 0, 1, 1, 1)
		self.grid.attach(self.comboboxtext2, 1, 1, 1, 1)

		self.comboboxtext3 = Gtk.ComboBoxText()
		self.comboboxtext3.append("DEFAULT", "-Sin diferenciador-")
		self.comboboxtext3.set_active_id("DEFAULT")
		for feature in features:
			self.comboboxtext3.append(f"{feature.upper()}", feature)

		self.grid.attach(Gtk.Label(label="Diferenciador"), 0, 2, 1, 1)
		self.grid.attach(self.comboboxtext3, 1, 2, 1, 1)

		self.comboboxtext4 = Gtk.ComboBoxText()
		self.comboboxtext4.append("DEFAULT", "winter")
		self.comboboxtext4.set_active_id("DEFAULT")
		cmaps = ["spring", "autumn", "summer", "cool"]
		for colormap in cmaps:
			self.comboboxtext4.append(f"{colormap.upper()}", colormap)

		self.grid.attach(Gtk.Label(label="Mapa de colores"),0, 3, 1, 1)
		self.grid.attach(self.comboboxtext4, 1, 3, 1, 1)

		# Se añade la matrix de comboboxtext a la ventana.
		self.box.pack_start(self.grid, True, True, 0)


	# Este metodo retorna el parametro asociado al eje x
	def get_x_axis(self):
		return self.comboboxtext1.get_active_text()


	# Este metodo retorna el parametro asociado al eje x
	def get_y_axis(self):
		return self.comboboxtext2.get_active_text()


	# Este metodo retorna el parametro asociado a la diferenciacion por color.
	def get_distinctive(self):
		if self.comboboxtext3.get_active_id() == "DEFAULT":
			return None
		else:
			return self.comboboxtext3.get_active_text()


	# Este metodo retorna el mapa de color a utilizar.
	def get_colormap(self):
		return self.comboboxtext4.get_active_text()


	# Este metodo levanta una ventana de advertencia
	# en caso de que 1 o mas entradas sean invalidas.
	def raise_warning(self):
		dialog = Gtk.MessageDialog(
								   parent=self,
								   flags=0,
								   message_type=Gtk.MessageType.WARNING,
								   buttons=Gtk.ButtonsType.CLOSE,
								   text="Entradas inválidas"
								   )
		dialog.format_secondary_text("Seleccione opciones válidas e intente de nuevo")
		response = dialog.run()
		dialog.destroy()
