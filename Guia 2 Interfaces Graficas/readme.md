# Programación Avanzada - Guía 2 Unidad 2



### Integrantes
* Magdalena Lolas [mlolas](https://gitlab.com/mlolas)
* Diego Núñez [diegonunezg](https://gitlab.com/diegonunezg)

---

_Esta aplicación abre un archivo de tipo json que contiene información de pacientes (Nombre, Apellido, Sexo, Edad, Peso, Altura) y los muestra por pantalla en un textview. Además, a partir de estos datos, la aplicación, a partir del análisis de los datos, genera una plantilla de informe que puede ser editada. La aplicación permite también, guardar el informe._


### Instrucciones de uso:
1. Clickear el botón abrir para cargar un archivo en la aplicación.
2. Seleccionar el archivo deseado. Esto hará que se carguen los datos en la app.
3. Leer el texto del informe y modificarlo si se requiere.
4. En caso de querer guardar el informe, oprimir el botón Guardar y seleccionar la ruta de guardado.
5. Si se desea limpiar los datos presentes en los textview, apretar el botón Limpiar.
6. Para cerrar la aplicación, presionar el botón x en la esquina superior derecha.

---

## Ventana Principal
La clase VentanaPrincipal, es una clase hija, que hereda de la clase padre Gtk.Window.
Con super() se instancia el constructor(\_\_init\_\_) de la clase padre, permitiendo que cualquier objeto creado a partir de la clase hija, presente las características o atributos que fueron heredados.
A partir de métodos propios de la clase Gtk.Window, se puede dar formato a la ventana, como por ejemplo, darle un titulo o cambiar el tamaño por defecto.

```
 ...
 12|class VentanaPrincipal(Gtk.Window):
 ...
 14|    def __init__(self):
 ...
 16|        super().__init__()
 ...
 19|        self.set_title("Analisis Medico")
 20|        self.set_default_size(600,600)
 ...

```

Dentro de esta clase, existen 3 métodos propios de la clase, que son llamados en el constructor.

```
 ...
 12|class VentanaPrincipal(Gtk.Window):
 ...
 14|    def __init__(self):
 ...
 33|        self.crear_toolbar()
 34|        self.crear_textview_data()
 35|        self.crear_textview_informe()
 ...

```

### crear_toolbar()
Este método se encarga de crear una toolbar en la parte superior de la ventana. Esta toolbar contiene 3 botones que realizan las principales acciones de la aplicación (Abrir, Guardar y Limpiar).

```
 ...
 12|class VentanaPrincipal(Gtk.Window):
 ...
 39|    def crear_toolbar(self):
 ...
 41|        toolbar =Gtk.Toolbar()
 ...
 43|        toolbar.set_orientation(Gtk.Orientation.HORIZONTAL)
 ...
 46|        self.grid.attach(toolbar, 0, 0, 3, 1)
 ...
 49|        boton_abrir_archivo = Gtk.ToolButton(label="Abrir")
 ...
 51|        boton_abrir_archivo.connect("clicked", self.filechooser_abrir)
 ...
 53|        toolbar.insert(boton_abrir_archivo, 0)
 ...
 56|        boton_guardar = Gtk.ToolButton(label="Guardar")
 ...
 58|        boton_guardar.connect("clicked", self.filechooser_guardar)
 ...
 60|        toolbar.insert(boton_guardar, 1)
 ...
 63|        boton_limpiar = Gtk.ToolButton(label="Limpiar")
 ...
 65|        boton_limpiar.connect("clicked", self.limpiar_textviews)
 ...
 67|        toolbar.insert(boton_limpiar, 2)
 ...

```

### crear_textview_data()
Este método se encarga de crear un textview que mostrará la información del archivo abierto. Este textview esta definido para que no pueda ser editado, en otras palabras es de solo lectura. Para identificarlo con el otro textview, se crea un objeto Gtk.Label para indicar en que columa se encuentra este textview.


```
 ...
 12|class VentanaPrincipal(Gtk.Window):
 ...
 71|    def crear_textview_data(self):
 ...
 74|        self.grid.attach(Gtk.Label(label="Datos"), 0, 1, 1, 1)
 ...
 77|        scrolled = Gtk.ScrolledWindow()
 ...
 80|        scrolled.set_hexpand(True)
 81|        scrolled.set_vexpand(True)
 ...
 84|        self.grid.attach(scrolled, 0, 2, 1, 1)
 ...
 87|        self.textview = Gtk.TextView()
 ...
 90|        self.textview.set_editable(False)
 91|        self.textview.set_cursor_visible(False)
 ...
 94|        self.buffer = self.textview.get_buffer()
 ...
 97|        scrolled.add(self.textview)
 ...

```

### crear_textview_informe()
Este método se encarga de crear un textview que mostrará el texto del informe generado. Este textview esta definido para que no pueda ser editado en un principio, cuando no hay información cargada en la aplicación. Luego, una vez que se cargo un archivo, y el informe fue generado, se permite la edición del mismo. Para diferenciarlo del otro textview, se crea un objeto Gtk.Label para indicar en que columa se encuentra.

```
 ...
 12|class VentanaPrincipal(Gtk.Window):
 ...
101|    def crear_textview_informe(self):
 ...
104|         self.grid.attach(Gtk.Label(label="Informe"), 1, 1, 1, 1)
 ...
107|         scrolled = Gtk.ScrolledWindow()
 ...
110|         scrolled.set_hexpand(True)
111|         scrolled.set_vexpand(True)
 ...
114|         self.grid.attach(scrolled, 1, 2, 1, 1)
 ...
117|         self.textview2 = Gtk.TextView()
 ...
122|         self.textview2.set_wrap_mode(Gtk.WrapMode.WORD)
 ...
125|         self.textview2.set_editable(False)
126|         self.textview2.set_cursor_visible(False)
 ...
129|         self.buffer2 = self.textview2.get_buffer()
 ...
132|         scrolled.add(self.textview2)
 ...

```

---

## Filechooser
Existen 2 instancias en las que se levanta una ventana de tipo Gtk.FileChooserDialog, al momento de abrir, y al momento de guardar un archivo.

> Al abrir

```
 ...
173|    def filechooser_abrir(self, widget):
174|        dialog = Gtk.FileChooserDialog(
175|                                       title="Elija un archivo para abrir",
176|                                       parent=self,
177|                                       action=Gtk.FileChooserAction.OPEN
178|        )
179|        dialog.add_buttons(
180|            Gtk.STOCK_CANCEL,
181|            Gtk.ResponseType.CANCEL,
182|            Gtk.STOCK_OPEN,
183|            Gtk.ResponseType.OK,
184|        )
 ...
187|        self.add_filters_open(dialog)
 ...
190|        response = dialog.run()
 ...
193|        if response == Gtk.ResponseType.OK:
194|            self.cargar_archivo(dialog.get_filename())
 ...
197|        dialog.destroy()
 ...

```

> Al guardar

```
201|    def filechooser_guardar(self, widget):
 ...
203|        start, end = self.buffer2.get_bounds()
204|        text = self.buffer2.get_text(start, end, False)
 ...
207|        if text:
208|            f_dialog = Gtk.FileChooserDialog(
209|                title="Elija donde guardar el archivo", parent=self, action=Gtk.FileChooserAction.SAVE
210|            )
211|            f_dialog.add_buttons(
212|                Gtk.STOCK_CANCEL,
213|                Gtk.ResponseType.CANCEL,
214|                Gtk.STOCK_SAVE,
215|                Gtk.ResponseType.OK,
216|            )
 ...
217|            self.add_filters_save(f_dialog)
 ...
221|            f_dialog.set_current_name(f"informe_.txt")
 ...
224|            response = f_dialog.run()
 ...
227|            if response == Gtk.ResponseType.OK:
228|                self.guardar_archivo(f_dialog.get_filename())
 ...
231|            f_dialog.destroy()
 ...

```

Se pueden apreciar ciertas diferencias entre ambos

Al abrir | Al guardar
---------|---------
En la construcción de la ventana, el parámetro action es de tipo Gtk.FileChooserAction.OPEN | En la construcción de la ventana, el parámetro action es de tipo Gtk.FileChooserAction.SAVE
Al añadir botones de stock se usa Gtk.STOCK_OPEN | Al añadir botones de stock se usa Gtk.STOCK_SAVE
  \- | Al momento de guardar, se define un nombre para el archivo por defecto con el método set_current_name().
