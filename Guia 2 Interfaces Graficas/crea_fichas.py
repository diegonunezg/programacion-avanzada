# By Diego Núñez y Magdalena Lolas

import random
import json

"""
El objetivo de este codigo, es generar archivos tipo json que sirvan para
la demostracion de la aplicacion desarrollada. Puesto que son creados
pensando en el formato que la aplicacion es capaz de procesar.

"""

# Se crean listas con datos que luego son seleccionados aleatoreamente
nombres_hombre = ["Juan", "Pedro", "Matias", "Felipe", "Diego", "Joaquin"]
nombres_mujer = ["Maria", "Ana", "Camila", "Sofia", "Macarena", "Valentina"]
apellidos = ["Soto", "Martinez", "Munoz", "Rodriguez", "Valdivia", "Torres"]


# Esta funcion crea un json a partir de la informacion generada
def crear_ficha(data):
    with open(f'{data["Nombre"]}_{data["Apellido"]}.json', "w") as archivo:
        json.dump(data, archivo)


# Se crean (3) sets de datos al azar
for x in range(3):
    sexo = random.choice(("M", "F"))
    if sexo == "M":
        nombre = random.choice(nombres_hombre)
    else:
        nombre = random.choice(nombres_mujer)
    apellido = random.choice(apellidos)
    peso = random.randint(50, 120)
    altura = random.randint(150, 190)
    edad = random.randint(18, 40)

    data = {
            "Nombre" : f"{nombre}",
            "Apellido" : f"{apellido}",
            "Sexo" : f"{sexo}",
            "Peso" : peso,
            "Altura" : altura,
            "Edad" : edad
           }

    crear_ficha(data)
