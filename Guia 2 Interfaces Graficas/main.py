# By: Diego Núñez y Magdalena Lolas

import json
import time
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


# Se define la clase VentanaPrincipal
class VentanaPrincipal(Gtk.Window):
    # Se define el constructor
    def __init__(self):
        # Se instancia el constructor de la super clase o clase padre
        super().__init__()

        # Se le añade un titulo y define un tamaño por defecto a la ventana
        self.set_title("Analisis Medico")
        self.set_default_size(600,600)
        # Al ser cerrada con el boton nativo, la app termina su ejecucion
        self.connect("destroy", Gtk.main_quit)

        # Se crea un widget Gtk.Grid para contener al resto de widgets
        self.grid = Gtk.Grid()
        # Se define la separacion entre celdas
        self.grid.set_column_spacing(6)
        self.grid.set_row_spacing(6)
        # Se añade el grid a la ventana
        self.add(self.grid)

        # Se llama a los metodos propios de la clase para crear el resto de widgets
        self.crear_toolbar()
        self.crear_textview_data()
        self.crear_textview_informe()


    # Este metodo crea una toolbar en la ventana
    def crear_toolbar(self):
        # Se crea el widget de tipo Gtk.Toolbar
        toolbar =Gtk.Toolbar()
        # Se le da una orientacion horizontal
        toolbar.set_orientation(Gtk.Orientation.HORIZONTAL)

        # Se añade la toolbar al grid en la posicion deseada
        self.grid.attach(toolbar, 0, 0, 3, 1)

        # Se crea un boton cuya funcion sera abrir un archivo
        boton_abrir_archivo = Gtk.ToolButton(label="Abrir")
        # Se conecta el boton con el metodo asociado al clickearlo
        boton_abrir_archivo.connect("clicked", self.filechooser_abrir)
        # El boton se inserta en la toolbar
        toolbar.insert(boton_abrir_archivo, 0)

        # Se crea un boton cuya funcion sera guardar un archivo
        boton_guardar = Gtk.ToolButton(label="Guardar")
        # Se conecta el boton con el metodo asociado al clickearlo
        boton_guardar.connect("clicked", self.filechooser_guardar)
        # El boton se inserta en la toolbar
        toolbar.insert(boton_guardar, 1)

        # Se crea un boton cuya funcion sera limpiar los textviews
        boton_limpiar = Gtk.ToolButton(label="Limpiar")
        # Se conecta el boton con el metodo asociado al clickearlo
        boton_limpiar.connect("clicked", self.limpiar_textviews)
        # El boton se inserta en la toolbar
        toolbar.insert(boton_limpiar, 2)


    # Este metodo crea el primer textview
    def crear_textview_data(self):

        # Se añade label que indica que el textview sera para mostrar datos
        self.grid.attach(Gtk.Label(label="Datos"), 0, 1, 1, 1)

        # Se crea un widget de tipo Gtk.ScrolledWindow
        scrolled = Gtk.ScrolledWindow()

        # Este widget se expande de forma horizontal y vertical
        scrolled.set_hexpand(True)
        scrolled.set_vexpand(True)

        # Se añade el widget al grid
        self.grid.attach(scrolled, 0, 2, 1, 1)

        # Se crea el widget textview
        self.textview = Gtk.TextView()

        # Este textview no es editable, muestra datos que no modificables
        self.textview.set_editable(False)
        self.textview.set_cursor_visible(False)

        # Se obtiene el buffer, es decir, la direccion en memoria del texto almacenado
        self.buffer = self.textview.get_buffer()

        # Se añade el textview a la scrolled window
        scrolled.add(self.textview)


    # Este metodo crea el segundo textview
    def crear_textview_informe(self):

        # Se añade label que indica que el textview sera para escribir el informe
        self.grid.attach(Gtk.Label(label="Informe"), 1, 1, 1, 1)

        # Se crea un widget de tipo Gtk.ScrolledWindow
        scrolled = Gtk.ScrolledWindow()

        # Este widget se expande de forma horizontal y vertical
        scrolled.set_hexpand(True)
        scrolled.set_vexpand(True)

        # Se añade el widget al grid
        self.grid.attach(scrolled, 1, 2, 1, 1)

        # Se crea el widget textview
        self.textview2 = Gtk.TextView()

        # Si el texto de una linea supera el limite vertical, muestra el resto
        # del contenido una linea mas abajo. Es meramente visual.
        # En memoria sigue estando en la misma linea.
        self.textview2.set_wrap_mode(Gtk.WrapMode.WORD)

        # En un principio no es editable, luego al cargar datos se activa
        self.textview2.set_editable(False)
        self.textview2.set_cursor_visible(False)

        # Se obtiene el buffer, es decir la direccion en memoria del texto almacenado
        self.buffer2 = self.textview2.get_buffer()

        # Se añade el textview a la scrolled window
        scrolled.add(self.textview2)


    # Este metodo limpia el texto contenido en ambos textviews
    def limpiar_textviews(self, widget):
        # Se obtiene el texto almacenado en ambos textviews
        start1, end1 = self.buffer.get_bounds()
        text1 = self.buffer.get_text(start1, end1, False)

        start2, end2 = self.buffer2.get_bounds()
        text2 = self.buffer2.get_text(start2, end2, False)

        # Esto solo ocurre en caso de que alguno de los dos textview tenga texto.
        # Ya que, si ambos estan vacios, carece de sentido limpiarlos.
        if text1 or text2:
            # Se levanta un dialogo, para confirmar la eliminacion del texto.
            dialog = Gtk.MessageDialog(
                                       parent=self,
                                       flags=0,
                                       message_type=Gtk.MessageType.QUESTION,
                                       buttons=Gtk.ButtonsType.YES_NO,
                                       text="El texto sera borrado"
                                       )
            dialog.format_secondary_text("¿Estas seguro de que deseas hacerlo?")

            # Se espera una respuesta del dialogo
            response = dialog.run()

            # Si la respuesta es si, el contenido de ambos textview es reseteado
            if response == Gtk.ResponseType.YES:
                self.buffer.set_text("")
                self.buffer2.set_text("")
                # El 2do textview ya no es editable, puesto que no hay datos
                self.textview2.set_editable(False)
                self.textview2.set_cursor_visible(False)

            # Finalmente, se destruye el dialogo independientemente de la respuesta
            dialog.destroy()


    # Este metodo levanta una ventana tipo filechooser para abrir un archivo
    def filechooser_abrir(self, widget):
        dialog = Gtk.FileChooserDialog(
                                       title="Elija un archivo para abrir",
                                       parent=self,
                                       action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        # Se añaden filtros de busqueda
        self.add_filters_open(dialog)

        # Se espera una respuesta de la ventana filechooser
        response = dialog.run()

        # Si la respuesta es del tipo OK se abre el archivo
        if response == Gtk.ResponseType.OK:
            self.cargar_archivo(dialog.get_filename())

        # Finalmente se destruye la ventana filechooser
        dialog.destroy()


    # Este metodo levanta una ventana tipo filechooser para guardar un archivo
    def filechooser_guardar(self, widget):
        # Se revisa el contenido del segundo buffer, ya que este representa el informe
        start, end = self.buffer2.get_bounds()
        text = self.buffer2.get_text(start, end, False)

        # Solo en caso de que haya texto se guarda, ya que sino carece de sentido
        if text:
            f_dialog = Gtk.FileChooserDialog(
                title="Elija donde guardar el archivo", parent=self, action=Gtk.FileChooserAction.SAVE
            )
            f_dialog.add_buttons(
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_SAVE,
                Gtk.ResponseType.OK,
            )
            # Se añaden filtros de busqueda
            self.add_filters_save(f_dialog)

            # Se establece un pre-nombre para guardar el archivo
            f_dialog.set_current_name(f"informe_.txt")

            # Se espera una respuesta de la ventana filechooser
            response = f_dialog.run()

            # Si la respuesta es del tipo OK, guarda el archivo
            if response == Gtk.ResponseType.OK:
                self.guardar_archivo(f_dialog.get_filename())

            # Finalmente la ventana filechooser se destruye
            f_dialog.destroy()


    # Este metodo crea el filtro para el filechooser que abre un archivo
    def add_filters_open(self, dialog):
        filter_json = Gtk.FileFilter()
        filter_json.set_name("json")
        filter_json.add_pattern("*.json")
        dialog.add_filter(filter_json)


    # Este metodo crea el filtro para el filechooser que guarda un archivo
    def add_filters_save(self, dialog):
        filter_txt = Gtk.FileFilter()
        filter_txt.set_name("txt")
        filter_txt.add_pattern("*.txt")
        dialog.add_filter(filter_txt)


    # Este metodo carga el archivo al primer textview
    def cargar_archivo(self, archivo=""):
        if archivo:
            # Se obtiene la info en forma de diccionario
            with open(archivo, 'r') as f:
                dicc = json.load(f)

            # Se empieza a crear el texto
            text = ""
            # Se vuelca cada llave y su contenido en el texto
            for key in dicc:
                text += "-" + key + ": " + f"{dicc[key]}" + "\n"

            # Se setea el primer textview a traves de su buffer
            self.buffer.set_text(text)
            # Se genera una plantilla de informe a partir de la data
            self.generar_informe(dicc)


    # Este metodo guarda el informe generado en un archivo nuevo
    def guardar_archivo(self, path):
        # Se obtiene el texto del textview a traves del buffer
        start, end = self.buffer2.get_bounds()
        text = self.buffer2.get_text(start, end, False)
        # Se guarda el archivo
        with open(path, 'w') as archivo:
            archivo.write(text)


    # Este metodo genera una plantilla de informe a partir de los datos
    def generar_informe(self, data):
        # Se obtiene la hora y fecha en la que se genero el informe
        lc = time.localtime(time.time())
        year = f"{lc.tm_year}"
        month = f"{lc.tm_mon}" if len(f"{lc.tm_mon}") == 2 else f"0{lc.tm_mon}"
        day = f"{lc.tm_mday}" if len(f"{lc.tm_mday}") == 2 else f"0{lc.tm_mday}"
        hour = f"{lc.tm_hour}" if len(f"{lc.tm_hour}") == 2 else f"0{lc.tm_hour}"
        min = f"{lc.tm_min}" if len(f"{lc.tm_min}") == 2 else f"0{lc.tm_min}"
        sec = f"{lc.tm_sec}" if len(f"{lc.tm_sec}") == 2 else f"0{lc.tm_sec}"

        if isinstance(data, dict):
            # Formulas de calculo de imc y peso
            # imc = peso / altura^2  -->  peso = imc * altura^2
            # peso(umbral) = imc(umbral) * altura^2

            imc = round(data['Peso']/((data['Altura']/100)**2), 2)

            if float(imc) < 18.5:
                umbral_peso = 18.5 * ((data['Altura']/100)**2)
                subir = round((umbral_peso - data['Peso']), 2)
                condicion_peso = "Bajo peso"
                observacion_peso = f"Debe subir {subir} kg para alcanzar un peso normal."

            elif 18.5 <= float(imc) and float(imc) < 25:
                umbral_peso_inf = 18.5 * ((data['Altura']/100)**2)
                umbral_peso_sup = 25 * ((data['Altura']/100)**2)
                bajar = round((data['Peso'] - umbral_peso_inf), 2)
                subir = round((umbral_peso_sup - data['Peso']), 2)
                condicion_peso = "Peso normal"
                observacion_peso = (f"Si baja {bajar} kg estara bajo peso. " +
                                    f"Si sube {subir} kg presentara sobrepeso.")

            elif 25 <= float(imc) and float(imc) < 30:
                umbral_peso_inf = 25 * ((data['Altura']/100)**2)
                umbral_peso_sup = 30 * ((data['Altura']/100)**2)
                bajar = round((data['Peso'] - umbral_peso_inf), 2)
                subir = round((umbral_peso_sup - data['Peso']), 2)
                condicion_peso = "Sobrepeso"
                observacion_peso = (f"Si baja {bajar} kg estara en un peso normal. " +
                                    f"Si sube {subir} kg presentara obesidad.")

            else:
                umbral_peso = 30 * ((data['Altura']/100)**2)
                umbral_peso_normal = 25 * ((data['Altura']/100)**2)
                bajar_a_sobrepeso = round((data['Peso'] - umbral_peso), 2)
                bajar_a_normal = round((data['Peso'] - umbral_peso_normal), 2)
                condicion_peso = "Obesidad"
                observacion_peso = (f"Si baja {bajar_a_sobrepeso} kg tendra sobrepeso. " +
                                    f"Para tener un peso normal, debe bajar {bajar_a_normal} kg.")

            # Toda la informacion obtenida se vuelca en la plantilla
            text = (
                    f"Fecha: {day}/{month}/{year}\n" +
                    f"Hora: {hour}:{min}:{sec}\n"
                    f"Paciente: {data['Nombre']} {data['Apellido']}\n" +
                    f"Edad: {data['Edad']}\n\n"
                    f"El paciente presenta un peso de {data['Peso']} kg y una " +
                    f"altura de {data['Altura']} cm, lo que indica que su " +
                    f"indice de masa corporal es de {imc} lo que quiere decir " +
                    f"que presenta {condicion_peso}. {observacion_peso}\n"
                    )

            # La plantilla del informe se carga en el textview
            self.buffer2.set_text(text)
            # El textview ahora puede ser editado para añadir o corregir datos
            self.textview2.set_editable(True)
            self.textview2.set_cursor_visible(True)



if __name__ == "__main__":
    window = VentanaPrincipal()
    window.show_all()

    Gtk.main()
