import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from ventana_dialogo import VentanaDialogo


# Clase que representa la ventana principal
class VentanaPrincipal(Gtk.Window):
    def __init__(self):
        super().__init__()
        # Se le entrega un titulo a la ventana, ademas de preestablecer su tamaño
        self.set_title("Sumatorias")
        self.set_default_size(800,600)
        # La ejecución del programa termina si la ventana es destruida
        self.connect("destroy", Gtk.main_quit)

        # Se crea un widget Gtk.Box con orientacion horizontal
        self.box = Gtk.Box()
        self.box.set_orientation(Gtk.Orientation.HORIZONTAL)

        # Se crea un widget Gtk.Box con orientacion vertical. Representa la columna 1
        self.col1 = Gtk.Box()
        self.col1.set_orientation(Gtk.Orientation.VERTICAL)

        # Se crean los widgets a introducir en la columna 1
        self.label1 = Gtk.Label()
        self.entry1 = Gtk.Entry()
        self.boton1 = Gtk.Button()

        # Se le establecen parametros a los widgets creados anteriormente
        self.label1.set_label("Sumando 1")
        self.boton1.set_label("Reset")
        self.boton1.connect("clicked", self.boton_reset_clicked)

        # Los widgets creados, son insertados en la columna 1
        self.col1.pack_start(self.label1, True, True, 0)
        self.col1.pack_start(self.entry1, True, True, 0)
        self.col1.pack_start(self.boton1, True, True, 0)

        # Se crea un widget Gtk.Box con orientacion vetical. Representa la columna 2
        self.col2 = Gtk.Box()
        self.col2.set_orientation(Gtk.Orientation.VERTICAL)

        # Se crean los widgets a introducir en la columna 2
        self.label2 = Gtk.Label()
        self.entry2 = Gtk.Entry()
        self.boton2 = Gtk.Button()

        # Se le establecen parametros a los widgets creados anteriormente
        self.label2.set_label("Sumando 2")
        self.boton2.set_label("Aceptar")
        self.boton2.connect("clicked", self.boton_aceptar_clicked)

        # Los widgets creados, son insertados en la columna 2
        self.col2.pack_start(self.label2, True, True, 0)
        self.col2.pack_start(self.entry2, True, True, 0)
        self.col2.pack_start(self.boton2, True, True, 0)

        # Ambas columnas son insertadas en el widget Gtk.Box principal
        self.box.pack_start(self.col1, True, True, 0)
        self.box.pack_start(self.col2, True, True, 0)

        # Se añade Gtk.Box principal a la ventana
        self.add(self.box)


    # Este metodo es llamado cuando se clickea el boton con label "Reset"
    # Su funcion es limpiar las entradas de texto (Gtk.Entry) de la ventana
    def boton_reset_clicked(self, widget):
        self.entry1.set_text("")
        self.entry2.set_text("")


    # Este metodo es llamado cuando se clickea el boton con label "Aceptar"
    # Su funcion es levantar una ventana de dialogo que muestra la suma.
    def boton_aceptar_clicked(self, widget):
        # Se crea el objeto tipo VentanaDialogo
        dialog = VentanaDialogo(self)

        # Se recoge la info ingresada en los Gtk.Entry
        sumando1 = self.entry1.get_text()
        sumando2 = self.entry2.get_text()

        # Dependiendo de si lo ingresado es numerico o no, efectua la suma de maneras diferentes
        if sumando1.isdigit():
            sumando1 = int(sumando1)
        else:
            sumando1 = len(sumando1)
        if sumando2.isdigit():
            sumando2 = int(sumando2)
        else:
            sumando2 = len(sumando2)

        # Se setea el label de la ventana de dialogo para que muestre el resultado
        dialog.label.set_label(f"{sumando1 + sumando2}")

        dialog.show_all()

        # Dependiendo del boton presionado, se toman unas acciones u otras.
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.entry1.set_text("")
            self.entry2.set_text("")
            dialog.destroy()
        elif response == Gtk.ResponseType.CANCEL:
            dialog.destroy()
        elif response == Gtk.ResponseType.DELETE_EVENT:
            dialog.destroy()
