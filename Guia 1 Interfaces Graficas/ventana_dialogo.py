import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


# Clase que representa una ventana de dialogo
class VentanaDialogo(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="Resultado", transient_for=parent, flags=0)

        # Se añaden 2 botones a la ventana
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        # Se le otorga un tamaño por defecto
        self.set_default_size(150, 100)
        # Se crea una label y se le añade a la ventana
        self.label = Gtk.Label()
        box = self.get_content_area()
        box.add(self.label)
