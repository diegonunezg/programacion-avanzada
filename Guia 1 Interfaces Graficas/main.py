# By Magdalena Lolas y Diego Núñez

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from ventana_main import VentanaPrincipal


def main():
    ventana_principal = VentanaPrincipal()
    ventana_principal.show_all()
    Gtk.main()


if __name__ == "__main__":
    main()
