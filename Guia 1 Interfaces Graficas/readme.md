# Programación Avanzada - Guía 1 Unidad 2



### Integrantes
* Magdalena Lolas [mlolas](https://gitlab.com/mlolas)
* Diego Núñez [diegonunezg](https://gitlab.com/diegonunezg)

---

_Esta aplicación consiste en una interfaz sencilla, que es capaz de realizar sumas de números y caracteres. El usuario puede ingresar un dato en cada uno de las dos entradas de texto y se le entregará como resultado la sumatoria de ambos datos. Si los datos son de tipo string, la interfaz calculará la cantidad de caracteres de la string, y usando este numero entero, calculara el resultado de la sumatoria de ambos datos ingresados._

### Instrucciones de uso:
1. Ingresar los datos que desea sumar en cada una de las dos entradas de texto.
2. Si desea borrar ambas entradas presione el botón Reset.
3. Presione el botón Aceptar para ver el resultado de la sumatoria, esto abre una ventana de dialogo que muestra el resultado.
4. Para volver atrás presione el botón Cancelar de la ventana de dialogo y podrá cambiar sus datos.
5. Para realizar otra sumatoria con nuevos datos presione el botón Aceptar nuevamente.
6. Para salir de la aplicación presione la x en la esquina superior derecha.

---

## Ventana Principal
La clase VentanaPrincipal, es una clase hija, que hereda de la clase padre Gtk.Window.
Con super() se instancia el constructor(\_\_init\_\_) de la clase padre, permitiendo que cualquier objeto creado a partir de la clase hija, presente las características o atributos que fueron heredados.
A partir de métodos propios de la clase Gtk.Window, se puede dar formato a la ventana, como por ejemplo, darle un titulo o cambiar el tamaño por defecto.

```
# ventana_main.py

 ...
 08|class VentanaPrincipal(Gtk.Window):
 09|    def __init__(self):
 10|        super().__init__()
 ...
 12|        self.set_title("Sumatorias")
 13|        self.set_default_size(800,600)
 ...

```

Dentro de esta ventana, se pueden anexar otros objetos de Gtk, también llamados widgets. En esta aplicación, se usan Boxes, Labels, Entrys y Buttons

### Boxes

Un objeto Gtk.Box, corresponde a un contenedor de elementos, es decir, permite insertar dentro de si otros widgets.
En la aplicación desarrollada, se crea una estructura que representa una matriz bidimensional. Existe una box en dirección horizontal, que contiene 2 boxes en su interior en dirección vertical, representando 2 columnas, y en cada columna se introduce un label, un entry y un botón.

```
# ventana_main.py

 ...
  8|class VentanaPrincipal(Gtk.Window):
  9|    def __init__(self):
 ...
 18|        self.box = Gtk.Box()
 19|        self.box.set_orientation(Gtk.Orientation.HORIZONTAL)
...
 22|        self.col1 = Gtk.Box()
 23|        self.col1.set_orientation(Gtk.Orientation.VERTICAL)
 ...
 41|        self.col2 = Gtk.Box()
 42|        self.col2.set_orientation(Gtk.Orientation.VERTICAL)
 ...

```

### Labels

Un objeto de tipo Gtk.Label, representa una etiqueta. Esta label, toma una variable de tipo string, para plasmarla en la ventana.
En la ventana principal existen 2 labels, uno por cada columna, situados sobre los widgets de tipo Gtk.Entry, indicando que estos entrys, reciben los sumandos 1 y 2.

```
# ventana_main.py

  ...
   8|class VentanaPrincipal(Gtk.Window):
   9|    def __init__(self):
  ...
  26|        self.label1 = Gtk.Label()
  ...
  31|        self.label1.set_label("Sumando 1")
  ...
  45|        self.label2 = Gtk.Label()
  ...
  50|        self.label2.set_label("Sumando 2")
  ...

```

### Entrys
Un objeto de tipo Gtk.Entry, representa una entrada de texto.
En el contexto de la app desarrollada, se utilizaron 2 de estos widgets, uno en cada columna.
Cada uno recibe el input del usuario que corresponden a los sumandos 1 y 2 que se desean sumar.

```
# ventana_main.py

 ...
  8|class VentanaPrincipal(Gtk.Window):
  9|    def __init__(self):
 ...
 27|        self.entry1 = Gtk.Entry()
 ...
 46|        self.entry2 = Gtk.Entry()
 ...

```

Los valores registrados en estas entrys, luego son rescatados a la hora de procesar la suma. Esto se hace a través del método get_text()


### Buttons

Un objeto de tipo Gtk.Button, representa un botón, que puede ser clickeado y desencadenar una acción. La acción realizada, depende de la señal que emita el botón. Un botón puede emitir señales OK, CANCEL, entre otros.

En la aplicación desarrollada, se utilizan los botones en la ventana principal, uno que cumple la función de resetear las entry, y otro que abre una ventana de dialogo que muestra el resultado de la suma.

```
# ventana_main.py

 ...
  8|class VentanaPrincipal(Gtk.Window):
  9|    def __init__(self):
 ...
 28|        self.boton1 = Gtk.Button()
 ...
 33|        self.boton1.connect("clicked", self.boton_reset_clicked)
 ...
 47|        self.boton2 = Gtk.Button()
 ...
 52|        self.boton2.connect("clicked", self.boton_aceptar_clicked)
 ...

```

_self.boton_reset_clicked_  y _self.boton_aceptar_clicked_, hacen referencia a métodos que son modelados en el código para que cumplan funciones específicas.

---

## Ventana de diálogo
La clase VentanaDialogo, es una clase hija, que hereda de la clase padre Gtk.Dialog.
Las ventanas de diálogo son una forma de mostrar al usuario una pequeña cantidad de información.
En este caso, se utilizó para mostrar el resultado de la sumatoria en un label, y además se le agregaron 2 botones, uno para Aceptar y otro para Cancelar.

```
# ventana_dialogo.py

 ...
  7|class VentanaDialogo(Gtk.Dialog):
  8|    def __init__(self, parent):
  9|        super().__init__(title="Resultado", transient_for=parent, flags=0)
 ...
 12|        self.add_buttons(
 13|            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
 14|            Gtk.STOCK_OK, Gtk.ResponseType.OK
 15|        )
 ...
 18|        self.set_default_size(150, 100)
 ...
 20|        self.label = Gtk.Label()
 21|        box = self.get_content_area()
 22|        box.add(self.label)
 23|

```
