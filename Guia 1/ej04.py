# Por: Magdalena Lolas y Diego Nuñez

# Se define una clase tipoo planeta
class Planeta():
	# Se define el constructor
	def __init__(self, nombre, posicion, masa, densidad, diametro, dist_sol):
		self.nombre = nombre
		self.posicion = posicion
		self.masa = masa
		self.densidad = densidad
		self.diametro = diametro
		self.dist_sol = dist_sol
	
	
	# Metodo que muestra la informacion de los objetos
	def mostrar_info(self):
		print(f"\nNombre : {self.nombre}")
		print(f"Posicion : {self.posicion}")
		print(f"Masa : {self.masa}")
		print(f"Densidad : {self.densidad}")
		print(f"Diametro : {self.diametro}")
		print(f"Distancia al Sol : {self.dist_sol}\n")
		 

# Funcion principal del programa
def main():
	# Se crean 8 objetos tipo planeta
 	mercurio = Planeta("Mercurio", 1, "3.285 x 10^23 kg", "5.43 g/cm^3", "4879.4 km", "0.387 UA")
 	venus =  Planeta("Venus", 2, "4.867 x 10^24 kg", "5.24 g/cm^3", "12104 km", "0.722 UA")
 	tierra =  Planeta("Tierra", 3, "5.972 x 10^24 kg", "5.51 g/cm^3", "12742 km", "1 UA")
 	marte =  Planeta("Marte", 4, "6.39 x 10^23 kg", "3.93 g/cm^3", "6779 km", "1.52 UA")
 	jupiter =  Planeta("Jupiter", 5, "1.898 x 10^27 kg", "1.33 g/cm^3", "139820 km", "5.2 UA")
 	saturno =  Planeta("Saturno", 6, "5.683 x 10^23 kg", "0.687 g/cm^3", "116460 km", "9.54 UA")
 	urano =  Planeta("Urano", 7, "8.681 x 10^25 kg", "1.27 g/cm^3", "50724 km", "19.22 UA")
 	neptuno =  Planeta("Neptuno", 8, "1.024 x 10^26 kg", "1.64 g/cm^3", "49244 km", "30.06 UA")
 	
 	# Menu de planetas
 	while True:
 		print("\nMenu de planetas | Opciones: \n")
 		print("< 1 > Mercurio")
 		print("< 2 > Venus")
 		print("< 3 > Tierra")
 		print("< 4 > Marte")
 		print("< 5 > Jupiter")
 		print("< 6 > Saturno")
 		print("< 7 > Urano")
 		print("< 8 > Neptuno")
 		print("< 9 > Salir del programa")
 		
 		try:
 			# Se le pide al usuario elegir una opcion
 			opcion = int(input("\nElija un planeta para ver sus datos: "))
 			if opcion < 1 or opcion > 9:
 				print("\nEl numero escogido escapa del rango. Por favor elija un numero de la lista.")
 				continue
 		except ValueError:
 			print("\nOpcion ingresada invalida. Intentelo de nuevo.")
 			continue
 		
 		#  Se gestionan las opciones
 		if opcion == 1:
 			mercurio.mostrar_info()
 		elif opcion == 2:
 			venus.mostrar_info()
 		elif opcion == 3:
 			tierra.mostrar_info()
 		elif opcion == 4:
 			marte.mostrar_info()
 		elif opcion == 5:
 			jupiter.mostrar_info()
 		elif opcion == 6:
 			saturno.mostrar_info()
 		elif opcion == 7:
 			urano.mostrar_info()
 		elif opcion == 8:
 			neptuno.mostrar_info()
 		else:
 			break
 	
if __name__ == "__main__":
	main()
	
		
