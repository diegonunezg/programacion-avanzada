# Por: Magdalena Lolas y Diego Nuñez

# Se define la clase Perro
class Perro():
	# Se define el constructor
	def __init__(self, nombre, hidratacion, pasear):
		self.nombre = nombre
		self.hidratacion = hidratacion
		self.pasear = pasear
		
	# Se define el metodo para tomar agua
	def set_tomar_agua(self):
	
		# Si el perro no esta pasenado:
		if not self.pasear:
			while True:
				try:
					# Se pide al usuario ingresar hace cuanto bebio agua el perro
					horas = int(input("Ingrese el numero de horas que han pasado desde que tomo agua el perro: "))
					# La cantidad de horas no puede ser negativa
					if horas < 0:
						print("El numero debe ser un entero positivo")
						continue
					self.hidratacion = horas
					break
				except ValueError:
					print("El numero ingresado debe ser entero")
		# Si el perro esta paseando, no puede tomar agua
		else:
			print("El perro no pudo tomar agua porque esta paseando")
	
	
	# Se define el metodo que entrega la cantidad de horas que han pasado desde que el perro bebio agua
	def get_hora_toma_agua(self):
		return self.hidratacion


	# Se defie el metodo que cambia el estado de paseo del perro
	def caminar(self):
		# Caso donde el perro no esta paseando
		if not self.pasear:
			# Caso donde el perro bebio hace menos de 4 horas
			if self.hidratacion <= 4:
				self.pasear = not self.pasear
				print(self.nombre, " salio a pasear")
			# Si no bebio hace menos de 4 horas no puede ir a pasear
			else:
				print(self.nombre, "no salio ya que no ha bebido en 4 horas")
		# Caso donde el perro esta paseando
		else:
			self.pasear = not self.pasear
			print(self.nombre, " volvio de su paseo")


# Funcion principal del programa
def main():
	while True:
		try:
			# Se pide ingresar el nombre del perro
			nombre = str(input("Ingrese el nombre del perro: "))
			break
		except ValueError:
			print("El dato ingresado no es correcto. Intente de nuevo.")
	while True:	
		try:
			# Se le pide al usuario ingresar la cantidad de horas que han pasado desde que bebio agua el perro
			h2o = int(input("Ingrese la cantidad de horas como numero entero, que han pasado desde que el perro bebio agua: "))
			break
		except ValueError:
			print("El dato ingresado no es correcto. Intente de nuevo.")
	# Se crea un objeto tipo perro
	perro = Perro(nombre, h2o, False)
	
	# Se muestra por pantalla un menu de objetos
	while True:
		print("Menu de opciones:\n")
		print("< 1 > Darle agua al perro")
		print("< 2 > Consultar hace cuanto tiempo tomo agua el perro")
		print("< 3 > Sacar a pasear al perro")
		print("< 4 > Cerrar el programa")
		
		while True:
			# Se le pide al usuario ingresar una opcion
			try:
				opcion = int(input("\nElija una opcion: "))
				break
			except ValueError:
				print("Ingrese un numero valido")
		}# Se toman las desiciones respectivas 
		if opcion == 1:
			perro.set_tomar_agua()
		elif opcion == 2:
			print(perro.get_hora_toma_agua())
		elif opcion == 3:
			perro.caminar()
		elif opcion == 4:
			break
		else:
			print("Elija una opcion valida")
	
if __name__ == "__main__":
	main()
