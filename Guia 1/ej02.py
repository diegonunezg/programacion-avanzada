# Por: Magdalena Lolas y Diego Nuñez

# Se define la clase Vacuna
class Vacuna():
	# Se define el constructor
	def __init__(self, nombre, lab):
		self.nombre = nombre
		self.lab = lab
		self.efectos = []
	
	
	# Se define el metodo para agregar efectos secudarios
	def set_agrega_efectos_secundarios(self):
		efecto = str(input("Ingrese un efecto secundario: "))
		self.efectos.append(efecto)
		
	
	# Se define el metodo para mostrar los efectos
	def mostrar_efectos_secundarios(self):
		cantidad = len(self.efectos)
		if cantidad == 0:
			return("Esta vacuna no tiene efectos secundarios registrados")
		else:
			return self.efectos
			

# Se define una funcion para imprimir por pantalla
def imprime_lista(dato):
	if type(dato) == str:
		print(dato)
	elif type(dato) == list:
		for item in dato:
			print(item)


# Funcion principal del programa
def main():
	# Se crean 3 objetos tipo Vacuna
	vac1 = Vacuna("Pfizer", "Pfizer")
	vac2 = Vacuna("Coronavac", "Sinovac")
	vac3 = Vacuna("AstraZeneca", "AstraZeneca")
	
	# Menu de vacunas
	while True:
		print("\nMenu de vacunas | Opciones: \n")
		print(f"< 1 > Nombre: {vac1.nombre} | Laboratorio: {vac1.lab}")
		print(f"< 2 > Nombre: {vac2.nombre} | Laboratorio: {vac2.lab}")
		print(f"< 3 > Nombre: {vac3.nombre} | Laboratorio: {vac3.lab}")
		print("< 4 > Salir del programa")
		
		try:
			# Se le pide al usuario elegir una opcion
			opcion = int(input("\nElige una opcion: "))
		except ValueError:
			print("\nLo ingresado no es valido. Intentelo de nuevo")
			continue
		
		if opcion == 4:
			break
		
		# Menu de opciones
		while True:
			print("\nMenu de opciones: \n")
			print("< 1 > Añadir un efecto secundario a la vacuna")
			print("< 2 > Mostrar efectos secundarios de la vacuna")
			print("< 3 > Volver al menu de seleccion de vacunas")
			
			try:
				# Se le pide al usuario elegir una opcion
				eleccion = int(input("\nElija una opcion: "))
			except ValueError:
				print("\nLo ingresado no es valido. Intentelo de nuevo")
			
			# Se gestionan las opciones
			if eleccion == 3:
				break
			
			if opcion == 1:
				if eleccion == 1:
					vac1.set_agrega_efectos_secundarios()
				elif eleccion == 2:
					imp = vac1.mostrar_efectos_secundarios()
					imprime_lista(imp)
				 
			elif opcion == 2:
				if eleccion == 1:
					vac2.set_agrega_efectos_secundarios()
				elif eleccion == 2:
					imp = vac2.mostrar_efectos_secundarios()
					imprime_lista(imp)
							
			elif opcion == 3:
				if eleccion == 1:
					vac3.set_agrega_efectos_secundarios()
				elif eleccion == 2:
					imp = vac3.mostrar_efectos_secundarios()
					imprime_lista(imp)
				
		
		
	
if __name__ == "__main__":
	main()
