import alumno as a
import plant as p
import random as r
from violeta import Violeta
from hierba import Hierba
from rabano import Rabano
from trebol import Trebol

# Se define la clase garden
class Garden():
    # Constructor
    def __init__(self):
        self._alumnos = []
        self._fila1 = []
        self._fila2 = []

    # Se definen metodos set y get para el atributo self._alumnos
    @property
    def alumnos(self):
        return self._alumnos
    @alumnos.setter
    def alumnos(self, alumno):
        if isinstance(alumno, a.Alumno):
            self._alumnos.append(alumno)
        else:
            raise ValueError(f"{alumno} no es un objeto de tipo Alumno")


    # Metodo que reparte objetos tipo plant a los objetos tipo alumno
    def repartir_plantas(self):
        for kid in self._alumnos:
            for x in range(0, 4):
                tmp = r.randint(1, 4)
                if tmp == 1:
                    plantita = Violeta()
                elif tmp == 2:
                    plantita = Hierba()
                elif tmp == 3:
                    plantita = Trebol()
                else:
                    plantita = Rabano()

                plantita.owner = kid
                kid.vasitos = plantita
                if x in (0, 1):
                    self._fila1.append(plantita)
                elif x in (2, 3):
                    self._fila2.append(plantita)


    # Metodo que retorna las plantas de un alumno en especifico
    def plantas(self, alumno):
        if isinstance(alumno, a.Alumno):
            return alumno.vasitos
        else:
            raise ValueError(f"{alumno} no es un objeto de tipo Alumno")


    # Metodo que imprime por pantalla las plantas que hay en la ventana
    def ventana(self, persona = None):
        # Imprime solo las plantas de un alumno en especifico
        if isinstance(persona, a.Alumno):
            text1, text2 = "", ""
            for x in range(0, len(self._alumnos)):
                if x == self._alumnos.index(persona):
                    text1 += persona.vasitos[0].nombre[0] + persona.vasitos[1].nombre[0]
                    text2 += persona.vasitos[2].nombre[0] + persona.vasitos[3].nombre[0]
                else:
                    text1 += ".."
                    text2 += ".."
                text1 += " "
                text2 += " "
            print(f"Las plantas de {persona.nombre} son:\n")

        # Imprime todas las plantas en la ventana
        else:
            l1 = list(map(lambda x: x.nombre[0], self._fila1))
            l2 = list(map(lambda x: x.nombre[0], self._fila2))
            text1 = ""
            text2 = ""
            for x in range(0, len(l1), 2):
                text1 += l1[x] + l1[x+1] + " "
                text2 += l2[x] + l2[x+1] + " "
        print("[ventana]"*4)
        print(text1)
        print(text2)
