from plant import Plant

# Clase Rabano hija de Plant
class Rabano(Plant):
    # Constructor
    def __init__(self):
        super().__init__()
        self._nombre = "Rabano"
