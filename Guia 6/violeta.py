from plant import Plant

# Clase Violeta hija de Plant
class Violeta(Plant):
    # Constructor
    def __init__(self):
        super().__init__()
        self._nombre = "Violeta"
