from plant import Plant

# Clase Hierba hija de Plant
class Hierba(Plant):
    # Constructor
    def __init__(self):
        super().__init__()
        self._nombre = "Hierba"
