import plant as p

# Se define la clase Alumno
class Alumno():
    # Constructor
    def __init__(self, nombre):
        if isinstance(nombre, str):
            self._nombre = nombre
        else:
            raise ValueError(f"{nombre} no es de tipo string")

        self._vasitos = []

    # Se definen un metodo get para el atributo self._nombre
    @property
    def nombre(self):
        return self._nombre

    # Se definen metodos set y get para el atributo self._vasitos
    @property
    def vasitos(self):
        return self._vasitos
    @vasitos.setter
    def vasitos(self, plantita):
        if isinstance(plantita, p.Plant):
            if len(self._vasitos) < 4:
                self._vasitos.append(plantita)
        else:
            raise ValueError(f"{plantita} no es un objeto tipo Plant")
